// drawAll.cpp : Defines the entry point (16 points) for the console application.
//Shaon D Gupta
//CSc 472 Fall'13
// Instructor: Prof. Dr. George Wolberg
//Date: 30 September 2013

#include "stdafx.h"
#include <GL/glut.h>

/* function prototypes */
void init	(void);
void display	(void);
void reshape	(int, int);

/* user-defined abstract datatype */
typedef float	vector2f[2];

/* global variables: window dimensions and vertex list */
int		WinW, WinH;
vector2f	p[] = {{0.05, 0.55}, {0.05, 0.60}, {0.05, 0.65}, {0.05, 0.70}, {0.05, 0.75}, 
                   {0.05, 0.80}, {0.05, 0.85}, {0.05, 0.90}, {0.05, 0.95}, {0.10, 0.945},
                   {0.15, 0.90}, {0.18,0.87}, {0.18, 0.83}, {0.15, 0.80}, {0.10, 0.74},
                   {0.05,0.74}};


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * main:
 *
 * Initialize window info, state variables, and callback functions.
 * Enter main loop and process events.
 */
int main(int argc, char** argv)
{
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); /* double-buffer; RGB    */
	glutInitWindowSize(500, 500);		/* initialize window size     */
	glutInitWindowPosition(100, 100);	/* initialize window position */
	glutCreateWindow(argv[0]);		/* set titlebar name; create  */
	init();					/* initialize state variables */
	glutDisplayFunc(display);		/* display callback function  */
	glutReshapeFunc(reshape);		/* reshape callback function  */
	glutMainLoop();				/* enter infinite event loop  */
	return 0;				/* ANSI C requires int main() */
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * init:
 *
 * Initialize state variables.
 * 
 */
void init(void) 
{
	glClearColor(0.0, 0.0, 0.0, 0.0);	/* set background color */
	glColor3f   (1.0, 1.0, 1.0);		/* set foreground color */

	/* initialize viewing values; setup unit view cube */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * display:
 *
 * Display callback function.
 */
void display(void)
{
	int	i, w3, h3;

	/* init w3 and h3 with dimensions of a window quadrant */
	w3 = WinW/3;// >> 1;
	h3 = WinH /3;//>> 1;

	glClear(GL_COLOR_BUFFER_BIT);		/* clear FB with backgd color */

	/* draw points */
	glViewport(0, 2*h3 , w3, h3);		/* draw into upper left quad  */
	glBegin(GL_POINTS);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* draw lines */
	glViewport(w3, 2*h3, w3, h3);		/* draw into upper middle quad */
	glBegin(GL_LINES);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* draw line strip */
	glViewport(2*w3, 2*h3, w3, h3);		/* draw into upper right quad */
	glBegin(GL_LINE_STRIP);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* flip next drawing (upside down) */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 1.0, 0.0, -1.0, 1.0);
	/* draw line loop */
	glViewport(0, h3, w3, h3);		/* draw into middle left  quad */
	glBegin(GL_LINE_LOOP);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* restore orientation of next five drawings */
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	/* draw triangle*/
	glViewport(w3, h3, w3, h3);		/* draw into middle  quad */
	glBegin(GL_TRIANGLES);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* draw triangle_strip*/
	glViewport(2*w3, h3, w3, h3);		/* draw into middle right  quad */
	glBegin(GL_TRIANGLE_STRIP);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* draw triangle_fan*/
	glViewport(0, 0, w3, h3);		/* draw into lower left  quad */
	glBegin(GL_TRIANGLE_FAN);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* draw quads*/
	glViewport(w3, 0, w3, h3);		/* draw into lower middle  quad */
	glBegin(GL_QUADS);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();

	/* draw polygon*/
	glViewport(2*w3, 0, w3, h3);		/* draw into lower right quad */
	glBegin(GL_POLYGON);
		for(i=0; i<16; i++) glVertex2fv(p[i]);
	glEnd();


   	glFlush();				/* draw to (off-screen) buffer*/
	glutSwapBuffers();			/* swap buffer to see drawing */
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * reshape:
 *
 * Reshape callback function.
 */
void reshape(int w, int h) 
{
	/* init global variables with window dimensions; for use in display() */
	WinW = w;
	WinH = h;

	/* initialize viewing values; setup unit view cube */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}