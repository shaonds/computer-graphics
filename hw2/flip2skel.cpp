// flip2_skel.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

// ======================================================================
//
// flip2.skel.cpp - Page flip simulation (with texture mapping).
//
// Written by:
//Shaon D Gupta
//CSc 472 Fall'13
// Instructor: Prof. Dr. George Wolberg
//Date: 28 October 2013

// ======================================================================
//

#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>
#include <GL/glut.h>
using namespace std;

// function prototypes
static void display	(void);
static void reshape	(int, int);
static void mouse	(int, int, int, int);
static void motion	(int, int);
static void init	(void);
static void drawLowerRightCorner1(int, int);
static void drawLowerRightCorner2(int, int);
static int  readPPM	(char*, int&, int&, unsigned char *&);


// global variables
int		W, H, Margin;
unsigned int	TexId1, TexId2;

// macros
#define CLIP(A,L,H)	((A)<=(L) ? (L) : (A)<=(H) ? (A) : (H))

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// main:
//
// Page flip simulation.
//
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(256, 256);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc	(display);
	glutReshapeFunc	(reshape);
	glutMouseFunc	(mouse);
	glutMotionFunc	(motion);
	glutMainLoop();
	return 0;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// init:
//
// Initialization routine before display loop.
//
static void init(void)
{
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glColor3f    (1.0, 1.0, 1.0);
	glClearColor (0.0, 0.0, 0.0, 1.0);

	// init global vars
	W = 0;
	H = 0;
	Margin = 40;

	// read first texture that will map onto first page
	int ww, hh;
	unsigned char *texData;
	readPPM("page1.ppm", ww, hh, texData);

	// init texture parameters
//..............
		glGenTextures  (1,&TexId1);
	glBindTexture  (GL_TEXTURE_2D, TexId1);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D   (GL_TEXTURE_2D, 0, 3, ww, hh, 0, GL_RGB,
			GL_UNSIGNED_BYTE, texData);


//...............
	// read second texture that will map onto second page
	readPPM("page2.ppm", ww, hh, texData);

	// init texture parameters
//......
	glGenTextures  (1,&TexId2);
	glBindTexture  (GL_TEXTURE_2D, TexId2);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D   (GL_TEXTURE_2D, 0, 3, ww, hh, 0, GL_RGB,
			GL_UNSIGNED_BYTE, texData);

//.......
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// display:
//
// Display handler routine.
//
static void display(void)
{
	// clear color and depth buffer to background values
	glClear(GL_COLOR_BUFFER_BIT);

	// enable texture mapping and bind first image to page
	//...
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, TexId1);
	//...
	// draw page outline
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);             glVertex2i(  Margin,   Margin);
		glTexCoord2f(0,1);             glVertex2i(  Margin, H-Margin);
		glTexCoord2f(1,1);             glVertex2i(W-Margin, H-Margin);
		glTexCoord2f(1,0);          	glVertex2i(W-Margin,   Margin);
	glEnd();

	// disable texture mapping
	//...
	glDisable(GL_TEXTURE_2D);
	//.....
	// flush display
	glutSwapBuffers();	
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// reshape:
//
// Reshape handler routine. Called after reshaping window.
//
static void reshape(int w, int h)
{
	W  = w;
	H  = h;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0., (double) w, (double) h, 0.);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// mouse:
//
// Mouse handler routine.
//
static void mouse(int button, int state, int x, int y)
{
	switch(button) {
	case GLUT_LEFT_BUTTON:
		glutPostRedisplay();
		break;
	default:
		break;
	}
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// motion:
//
// Mouse motion handler routine.
//
static void motion(int x, int y)
{
	if((H-Margin)-y > 0)	// pull corner above lower edge of page
		drawLowerRightCorner1(x, y);
	else	drawLowerRightCorner2(x, y);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// lowerRightCorner1:
//
// Draw folded triangle or quadrilateral when lower right corner is
// pulled ABOVE lower edge of page.
//
static void drawLowerRightCorner1(int x, int y)
{
	float	bottom[2], right[2], top[2], corner[2];

	// clear color and depth buffer to background values
	glClear(GL_COLOR_BUFFER_BIT);

	// enable texture mapping
	//......
	glEnable(GL_TEXTURE_2D);
	//.....
	// init page dimensions
	int w = W - 2*Margin;
	int h = H - 2*Margin;

	// flag default case: folded triangle at corner
	int triangle = 1;

	// clip x
	if(x > W-Margin) x = W-Margin;

	// distance between (x,y) and page boundary
	int dx = (W-Margin) - x;
	int dy = (H-Margin) - y;

	double len = sqrt((x-Margin)*(x-Margin) + dy*dy);
	if(dy < 0 || len >= w) return;

	//	 ____
	//       \dx|
	//      b \ | a   Lifted folded triangle above bottom right corner
	//         \|
	//
	// Note: a + b = dy (distance from top of triangle to bottom paper edge)
	// Plug  a = dy - b into:
	//	 dx^2 + a^2 = b^2
	// Solve for b to get:
	//	 b = (dx^2 + dy^2) / 2dy
	// 
	double b = (dx*dx + dy*dy) / (2*dy);
	double a =  dy - b;

	// eval intersection on right and bottom edges
	bottom[0] = x - a*dy/dx;
	bottom[1] = H-Margin;
	right [0] = W-Margin;
	right [1] = y + a;

	// error checking: avoid tearing bottom edge of paper from spine
	if(bottom[0] < Margin) {
		bottom[0] = Margin;		// clamp bottom x to spine
		x  = (int) sqrt(w*w - dy*dy);	// adjust lifted corner position
		x += Margin;			// add back the Margin pixels
	}

	// error checking: lifted quadrilateral, not triangle
	if(right[1] < Margin) {
		// flag quadrilateral case
		triangle = 0;

		// eval position of lifted top right corner
		double  d = sqrt(dx*dx + a*a);
		corner[0] = x + (h * dx/d);
		corner[1] = y + (h *  a/d);

		top[0] = corner[0] + ((corner[1]-Margin) * a/dx);
		top[1] = Margin;
	}

	// draw page outline
	if(triangle) {
		// draw main polygon
		//...		
		//..............
		// bind first texture
		//............
		glBindTexture(GL_TEXTURE_2D, TexId1);
		//......
		glBegin(GL_POLYGON);

		// draw bottom left, upper left, and upper right corners
		glTexCoord2f(0,1);		glVertex2f(Margin,H-Margin); //
		glTexCoord2f(0,0);		glVertex2f(Margin,Margin);  //
		glTexCoord2f(1,0);		glVertex2f(W-Margin,Margin);//

		// xsect at right edge
		glTexCoord2f(1,(right[1]-Margin)/h);//
		glVertex2f  (W-Margin,right[1]);

		// xsect at bottom edge
		glTexCoord2f((bottom[0]-Margin)/w, 1);
		glVertex2f(bottom[0],H-Margin);
		glEnd();

		// draw lifted triangle
		//??...				// bind second texture
		//....
		glBindTexture(GL_TEXTURE_2D, TexId2);
		//.......
		glBegin(GL_POLYGON);
		glTexCoord2f(0,1);
		glVertex2f  (x,y);		// lifted corner point

		// xsect at right edge
		glTexCoord2f(0,(right[1]-Margin)/h);//right[0],right[1]);  //...............
		glVertex2f  (W-Margin,right[1]);//.......?

		// xsect at bottom edge
		glTexCoord2f(((W-Margin)-bottom[0])/w, 1);
		glVertex2f  (bottom[0],H-Margin);
		glEnd();
	} else {
		// draw main polygon
		/////...		// bind first texture
		//.....
		glBindTexture(GL_TEXTURE_2D, TexId1);
		///.....
		glBegin(GL_POLYGON);

		// draw bottom left and upper left corners
		glTexCoord2f(0,1);		glVertex2f(Margin,H-Margin);
		glTexCoord2f(0,0);		glVertex2f(Margin,Margin);

		// xsect at top edge
		glTexCoord2f((top[0]-Margin)/w, 0);
		glVertex2f  (top[0],Margin);

		// xsect at bottom edge
		glTexCoord2f((bottom[0]-Margin)/w, 1);
		glVertex2f  (bottom[0],H-Margin);
		glEnd();

		// draw lifted quadrilateral
		/////		...				// bind second texture
		//...
		glBindTexture(GL_TEXTURE_2D, TexId2);
		//....
		glBegin(GL_POLYGON);

		// draw lifted corner point and next lifted corner
		glTexCoord2f(0,1);		glVertex2f(x,y);
		glTexCoord2f(0,1);		glVertex2f(corner[0],corner[1]);

		// xsect at right edge
		glTexCoord2f(((W-Margin)-top[0])/w, 0);
		glVertex2f  (top[0], Margin);

		// xsect at bottom edge
		glTexCoord2f(((W-Margin)-bottom[0])/w, 1);
		glVertex2f  (bottom[0], H-Margin);
		glEnd();
	}

	// disable texture mapping
	//...
	glDisable(GL_TEXTURE_2D);
	//....
	// flush to display
	glutSwapBuffers();
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// lowerRightCorner2:
//
// Draw folded quadrilateral when lower right corner is
// pulled BELOW lower edge of page.
//
static void drawLowerRightCorner2(int x, int y)
{
	float	bottom[2], right[2], top[2], corner[2];

	// clear color and depth buffer to background values
	glClear(GL_COLOR_BUFFER_BIT);

	// enable texture mapping
	//...
	glEnable(GL_TEXTURE_2D);
	//....
	// init page dimensions
	int w = W - 2*Margin;
	int h = H - 2*Margin;

	// clip x
	if(x > W-Margin) x = W-Margin;

	// distance between (x,y) and page boundary
	float dx = (W-Margin) - x;
	float dy = (H-Margin) - y;

	double len = sqrt((x-Margin)*(x-Margin) + dy*dy);
	if(dy > 0 || len >= w) return;

	//	  a
	//	____
	//	|  /
	//   dy | / b	  Lifted folded triangle below bottom right corner
	//	|/
	//
	// Note: a + b = dx (distance from left of triangle to right paper edge)
	// Plug  a = dx - b into:
	//	 dy^2 + a^2 = b^2
	// Solve for b to get:
	//	 b = (dx^2 + dy^2) / 2dx
	// 
	double b = (dx*dx + dy*dy) / (2*dx);
	double a =  dx - b;

	// eval intersection on bottom edge
	bottom[0] = x+a;
	bottom[1] = H-Margin;

// eval position of lifted top right corner
	corner[0] = x;
	corner[1] = y;

	dx = (W-Margin) - corner[0];
	dy =  corner[1] - Margin;
	b  = (dx*dx + dy*dy) / (2*dx);
	a  =  dx - b;

	// eval position of lifted top left corner
	top[0] = x - (h * dy/b);
	top[1] = y + (h * a/b);
	// eval position of lifted top right corner
/*	top[0] = x-((dy/b)*h);
	top[1] = y+((a/b)*h);*/

	dx = (W-Margin) - top[0];
	dy =  top[1] - Margin;
	b  = (dx*dx + dy*dy) / (2*dx);
	a  =  dx - b;

	// eval position of lifted top left corner
/*	top[0] = corner[0] + a;
	top[1] = Margin;*/

	// error checking: avoid tearing top edge of paper from spine
	if(top[0] < Margin) {
		corner[0] += (Margin-top[0]);	// reset lifted corner
		top[0] = Margin;		// clamp top x to spine
	}

	// error checking: avoid stretching
	if(corner[1] > W - Margin) return;//......//

	// draw page outline
	//
	//...		// bind first texture
	//....
	glBindTexture(GL_TEXTURE_2D, TexId1);
	//......
	glBegin(GL_POLYGON);

	// draw bottom left / upper left corners, and top / bottom xsects
	glTexCoord2f(0,1);		                 glVertex2f(Margin,H-Margin);
	glTexCoord2f(0,0);		                 glVertex2f(Margin,Margin);
	glTexCoord2f(((top[0]+a)-Margin)/w, 0);	 glVertex2f(top[0]+a, Margin);
	glTexCoord2f((bottom[0]-Margin)/w, 1);	 glVertex2f(bottom[0], H-Margin);
	glEnd();

	// draw lifted quadrilateral
	//??...				// bind second texture
	//...
	glBindTexture(GL_TEXTURE_2D, TexId2);
		//...
	glBegin(GL_POLYGON);

	// draw lifted corner point and next lifted corner
	glTexCoord2f(0,1);		glVertex2f(corner[0],corner[1]);
	glTexCoord2f(0,0);		glVertex2f(top[0],top[1]);

	// xsect at right edge
    glTexCoord2f(((W-Margin)-(top[0]+a))/w, 0);
	glVertex2f (top[0]+a, Margin);

	// xsect at bottom edge
	glTexCoord2f(((W-Margin)-bottom[0])/w, 1);
	glVertex2f (bottom[0], H-Margin);
	glEnd();

	// disable texture mapping
	//...
	glDisable(GL_TEXTURE_2D);
	//...
	// flush to display
	glutSwapBuffers();
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// readPPM:
//
// Read a PPM file.
// Pass back dimensions and image pointer in width, height, and image.
// Return 1 for success, or 0 for failure.
//
static int readPPM(char *file, int &width, int &height, unsigned char* &image)
{
	// open binary file for reading
	ifstream inFile(file, ios::binary);
	assert(inFile);

	// verify that the image is in PPM format
	// error checking: first two bytes must be code for a raw PPM file
	char buf[80];
	inFile.getline(buf, 3);
	if(strncmp(buf, "P6", 2)) {
		cerr << "The file " << file << " is not in PPM format)\n";
		inFile.close();
		return 0;
	}

	// read width, height, and maximum gray val
	int maxGray;
	inFile >> width >> height >> maxGray;

	// skip over linefeed and carriage return
	inFile.getline(buf, 2);

	// allocate image
	image = new unsigned char[width*height*3];
	assert(image);

	// read entire image data
	inFile.read((char*) image, width*height*3);
	inFile.close();
	return 1;
}
