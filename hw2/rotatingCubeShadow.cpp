// rotatingCubeShadow.cpp : Defines the entry point for the console application.
//Shaon D Gupta
//CSc 472 Fall'13
// Instructor: Prof. Dr. George Wolberg
//Date: 28 October 2013


#include "stdafx.h"
#include <stdlib.h>
#include <GL/glut.h>

GLfloat vertices[] = {-1.0,-1.0,-1.0,1.0,-1.0,-1.0,
	1.0,1.0,-1.0, -1.0,1.0,-1.0, -1.0,-1.0,1.0, 
	1.0,-1.0,1.0, 1.0,1.0,1.0, -1.0,1.0,1.0};

	GLfloat colors[] = {0.0,0.0,0.0,1.0,0.0,0.0,
	1.0,1.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0, 
	1.0,0.0,1.0, 1.0,1.0,1.0, 0.0,1.0,1.0};

	GLfloat bcolors[] = {0.0,0.0,0.0,0.0,0.0,0.0,
	0.0,0.0,0.0, 0.0,0.0,0.0, 0.0,0.0,0.0, 
	0.0,0.0,0.0, 0.0,0.0,0.0, 0.0,0.0,0.0};

    GLubyte cubeIndices[]={0,3,2,1,2,3,7,6,0,4,7,
		3,1,2,6,5,4,5,6,7,0,1,5,4};



static GLfloat theta[] = {0.0,0.0,0.0};
static GLint axis = 2;

void display(void)
{

	GLfloat lighta[3]={0.0, 20.0, 0.0}; //
	GLfloat lightb[3]={0.0, 0.0, 20.0}; //
	GLfloat lightc[3]={20.0, 0.0, 0.0}; //
	GLfloat m[16];//

    int i,j;//

/* display callback, clear frame buffer and z buffer,
   rotate cube and draw, swap buffers */

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//setup camera view
	glLoadIdentity(); //
	gluLookAt(1.0,1.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0);
	glPushMatrix();
	//------

	//draw a purple colored floor
	glBegin(GL_QUADS);
            glColor3f(.5,.0,.5);
            glVertex3f(0.0,0.0,0.0);
            glVertex3f(5.0,0.0,0.0);
            glVertex3f(5.0,0.0,5.0);
            glVertex3f(0.0,0.0,5.0);
    glEnd();
	//draw a green left wall
	glBegin(GL_QUADS);
            glColor3f(0.1,0.4,0.0);
            glVertex3f(0.0,0.0,0.0);
            glVertex3f(0.0,5.0,0.0);
            glVertex3f(0.0,5.0,5.0);
            glVertex3f(0.0,0.0,5.0);
    glEnd();
	//draw a blue right wall
	glBegin(GL_QUADS);
            glColor3f(0.0,0.4,0.7);
            glVertex3f(0.0,0.0,0.0);
            glVertex3f(5.0,0.0,0.0);
            glVertex3f(5.0,5.0,0.0);
            glVertex3f(0.0,5.0,0.0);
    glEnd();

	//draw cube
	glTranslatef(3.0, 3.0, 3.0);
	glRotatef(theta[0], 1.0, 0.0, 0.0);
	glRotatef(theta[1], 0.0, 1.0, 0.0);
	glRotatef(theta[2], 0.0, 0.0, 1.0);
    
    glColorPointer(3,GL_FLOAT, 0, colors); 
    glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);

	//draw first shadow on floor

	for(i=0;i<16;i++) m[i]=0.0;
	m[0]=m[5]=m[10]=1.0;
	m[7]=-1.0 / lighta[1];

	glPopMatrix();
	glPushMatrix();
	//remove flicker from shadow
	glTranslatef(3.0, 0.01, 3.0); //
	glTranslatef(lighta[0], lighta[1],lighta[2]);//
	glMultMatrixf(m);//
	glTranslatef(-lighta[0], -lighta[1],-lighta[2]);//
	glColor3f(0.0,0.0,0.0);//
	glRotatef(theta[0], 1.0, 0.0, 0.0);//
	glRotatef(theta[1], 0.0, 1.0, 0.0);//
	glRotatef(theta[2], 0.0, 0.0, 1.0);//

    glColorPointer(3,GL_FLOAT, 0, bcolors); 
 	glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);
	
	//drawing second shadow
	for(i=0;i<16;i++) m[i]=0.0;
	m[0]=m[5]=m[10]=1.0; 
	m[11]= -1.0 / lightb[2];

	glPopMatrix();
	glPushMatrix();
	//remove flicker from shadow
	glTranslatef(3.0, 3.0, 0.01); //
	glTranslatef(lightb[0], lightb[1],lightb[2]);//
	glMultMatrixf(m);//
	glTranslatef(-lightb[0], -lightb[1],-lightb[2]);//
	glColor3f(0.0,0.0,0.0);//
	glRotatef(theta[0], 1.0, 0.0, 0.0);//
	glRotatef(theta[1], 0.0, 1.0, 0.0);//
	glRotatef(theta[2], 0.0, 0.0, 1.0);//

	glColorPointer(3,GL_FLOAT, 0, bcolors); 
 	glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);

	//draw third shadow
	for(i=0;i<16;i++) m[i]=0.0;
	m[0]=m[5]=m[10]=1.0;
	m[3]= -1.0/ lightc[0];

	glPopMatrix();
	glPushMatrix();
	//remove flicker from shadow
	glTranslatef(0.01, 3.0, 3.0); //
	glTranslatef(lightc[0], lightc[1],lightc[2]);//
	glMultMatrixf(m);//
	glTranslatef(-lightc[0], -lightc[1],-lightc[2]);//
	glColor3f(0.0,0.0,0.0);//
	glRotatef(theta[0], 1.0, 0.0, 0.0);//
	glRotatef(theta[1], 0.0, 1.0, 0.0);//
	glRotatef(theta[2], 0.0, 0.0, 1.0);//

	glColorPointer(3,GL_FLOAT, 0, bcolors); 
 	glDrawElements(GL_QUADS, 24, GL_UNSIGNED_BYTE, cubeIndices);

	glutSwapBuffers(); 
}

void spinCube()
{

/* Idle callback, spin cube 2 degrees about selected axis */

	theta[axis] += 0.5;
	if( theta[axis] > 360.0 ) theta[axis] -= 360.0;
	glutPostRedisplay();
}

void mouse(int btn, int state, int x, int y)
{

/* mouse callback, selects an axis about which to rotate */

	if(btn==GLUT_LEFT_BUTTON && state == GLUT_DOWN) axis = 0;
	if(btn==GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) axis = 1;
	if(btn==GLUT_RIGHT_BUTTON && state == GLUT_DOWN) axis = 2;
}
//keyboard function 
void keyboard(unsigned char key, int x, int y)
{
	if (key == 'X' || key == 'x') axis = 0;
	if (key == 'Y' || key == 'y') axis = 1;
	if (key == 'Z' || key == 'z') axis = 2;
}
void myReshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w <= h)
        glOrtho(-4.0, 4.0, -4.0 * (GLfloat) h / (GLfloat) w,
            4.0 * (GLfloat) h / (GLfloat) w, -10.0, 10.0);
    else
        glOrtho(-4.0 * (GLfloat) w / (GLfloat) h,
            4.0 * (GLfloat) w / (GLfloat) h, -4.0, 4.0, -10.0, 10.0);
    glMatrixMode(GL_MODELVIEW);
}

void main(int argc, char **argv)
{


/* need both double buffering and z buffer */

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutCreateWindow("Rotating Cube Shadow");
    glutReshapeFunc(myReshape);
    glutDisplayFunc(display);
    glutIdleFunc(spinCube);
    glutMouseFunc(mouse);
	//adding keyboard function
    glutKeyboardFunc(keyboard); 

    glEnable(GL_DEPTH_TEST); /* Enable hidden--surface--removal */
 	glEnableClientState(GL_COLOR_ARRAY); 
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, vertices);
    glColorPointer(3,GL_FLOAT, 0, colors); 
	glClearColor(1.0,1.0,1.0,1.0);
	glColor3f(1.0,1.0,1.0);
    glutMainLoop();
}
